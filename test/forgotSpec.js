process.env.NODE_ENV='local';

var app = require('../api_server.js');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
//var nock = require('nock');
//var scope = nock('https://secure.my.server.com');




describe('/forgot', function() {

	describe('with nonexistant email', function() {

		it('should return correct code and data', function(done) {

			var forgotData = {
				'email' : 'domokennaressa@gmail.com'
			};

			request(app)
				.post('/forgot')
				.send(forgotData)
				// end handles the response
				.end(function(err, res) {
					if (err) {
						throw err;
					}
					else {
						res.body.code.should.equal(3);
						res.body.should.have.property('data');
						done();
					}
				});
		});

	});




});