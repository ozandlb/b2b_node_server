process.env.NODE_ENV='local';

var app = require('../api_server.js');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
//var nock = require('nock');
//var scope = nock('https://secure.my.server.com');


describe('Login', function() {

//	var url = 'https://127.0.0.1:8445';


	describe('empty password', function() {

		it('should return user name and session id', function(done) {
			var pData = {
				'email' : 'sm@jamb2b.com',
				'pword' : ''
			};

			request(app)

				.post('/login')
				.send(pData)
				// end handles the response
				.end(function(err, res) {
					if (err) {
						throw err;
					}

					res.body.code.should.equal(0);
					res.body.error.should.equal('Missing username or password');
					done();
				});
		});
	});


	describe('POST /login', function() {

		it('should return user name and session id', function(done) {
			var pData = {
				'email' : 'sm@jamb2b.com',
				'pword' : 'smsmsm'
			};

			request(app)

				.post('/login')
				.send(pData)
				// end handles the response
				.end(function(err, res) {
					if (err) {
						throw err;
					}

					res.body.code.should.equal(1);
					res.body.data.firstname.should.equal('Sudeep');
					res.body.data.lastname.should.equal('Mathur');
					res.body.data.should.have.property('sessionid');
					done();
				});
		});
	});
});
