process.env.NODE_ENV='local';

var app = require('../api_server.js');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
//var nock = require('nock');
//var scope = nock('https://secure.my.server.com');




describe('Get user list: GET /user', function() {

	describe('with correct session data', function() {

		it('should return code 1 and list of users', function(done) {

			var loginData = {
				'email' : 'sm@jamb2b.com',
				'pword' : 'smsmsm'
			};

			request(app)
				.post('/login')
				.send(loginData)
				// end handles the response
				.end(function(err, res) {
					if (err) {
						throw err;
					}
					else {
						request(app)
							.get('/users?sessionid='+res.body.data.sessionid)
							.end(function(err, res2) {
								if (err) {
									throw err;
								}
								res2.body.code.should.equal(1);
								res2.body.should.have.property('data');
								done();
							});
					}
				});
		});

	});




});