process.env.NODE_ENV='local';

var app = require('../api_server.js');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
//var nock = require('nock');
//var scope = nock('https://secure.my.server.com');





describe('Admin add user: POST /admin/user', function() {

	describe('with no session data passed', function() {

		it('should return error: invalid session', function(done) {

			request(app)

				.post('/admin/users')
//				.send(pData)
				// end handles the response
				.end(function(err, res) {
					if (err) {
						throw err;
					}

//					console.log(res.body);
					res.body.code.should.equal(900);
					res.body.error.should.equal('session expired');
					done();
				});
		});
	});



	describe('with incorrect session data', function() {

		it('should return error: invalid session', function(done) {

			request(app)

				.post('/admin/users?sessionid=123WrongSession123')
				.end(function(err, res) {
					if (err) {
						throw err;
					}

//					console.log(res.body);
					res.body.code.should.equal(900);
					res.body.error.should.equal('session expired');
					done();
				});
		});
	});





	describe('with correct session data but invalid email', function() {

		it('should return error: invalid email', function(done) {

			var loginData = {
				'email' : 'sm@jamb2b.com',
				'pword' : 'smsmsm'
			};

			request(app)

				.post('/login')
				.send(loginData)
				// end handles the response
				.end(function(err, res) {
					if (err) {
						throw err;
					}

					else {

						var userData = {
							'email' : 'sm@ haha.c',
							'firstname' : 'Billy',
							'lastname' : 'Bobby'
						};

						request(app)

							.post('/admin/users?sessionid='+res.body.data.sessionid)
							.send(userData)
							.end(function(err, res2) {
								if (err) {
									throw err;
								}

//								console.log(res2.body);
								res2.body.code.should.equal(2);
								res2.body.error.should.equal('invalid email');
								done();
							});
					}
				});
		});
	});



	describe('with correct session and email but invalid first name', function() {

		it('should return error: invalid name', function(done) {

			var loginData = {
				'email' : 'sm@jamb2b.com',
				'pword' : 'smsmsm'
			};

			request(app)

				.post('/login')
				.send(loginData)
				// end handles the response
				.end(function(err, res) {
					if (err) {
						throw err;
					}

					else {

						var userData = {
							'email' : 'sm@haha.com',
							'firstname' : 'Bill@y',
							'lastname' : 'Bobby'
						};

						request(app)

							.post('/admin/users?sessionid='+res.body.data.sessionid)
							.send(userData)
							.end(function(err, res2) {
								if (err) {
									throw err;
								}

//								console.log(res2.body);
								res2.body.code.should.equal(3);
								res2.body.error.should.equal('invalid name');
								done();
							});
					}
				});
		});
	});



	describe('with correct session and email but invalid last name', function() {

		it('should return error: invalid name', function(done) {

			var loginData = {
				'email' : 'sm@jamb2b.com',
				'pword' : 'smsmsm'
			};

			request(app)

				.post('/login')
				.send(loginData)
				// end handles the response
				.end(function(err, res) {
					if (err) {
						throw err;
					}

					else {

						var userData = {
							'email' : 'sm@haha.com',
							'firstname' : 'Billy',
							'lastname' : 'Bob#by'
						};

						request(app)

							.post('/admin/users?sessionid='+res.body.data.sessionid)
							.send(userData)
							.end(function(err, res2) {
								if (err) {
									throw err;
								}

//								console.log(res2.body);
								res2.body.code.should.equal(3);
								res2.body.error.should.equal('invalid name');
								done();
							});
					}
				});
		});
	});



	describe('with correct session and email but invalid last name', function() {

		it('should return error: invalid name', function(done) {

			var loginData = {
				'email' : 'sm@jamb2b.com',
				'pword' : 'smsmsm'
			};

			request(app)

				.post('/login')
				.send(loginData)
				// end handles the response
				.end(function(err, res) {
					if (err) {
						throw err;
					}

					else {

						var userData = {
							'email' : 'sm@haha.com',
							'firstname' : 'Billy',
							'lastname' : 'Bob#by'
						};

						request(app)

							.post('/admin/users?sessionid='+res.body.data.sessionid)
							.send(userData)
							.end(function(err, res2) {
								if (err) {
									throw err;
								}

//								console.log(res2.body);
								res2.body.code.should.equal(3);
								res2.body.error.should.equal('invalid name');
								done();
							});
					}
				});
		});
	});



	describe('with valid session and user first name and last name', function() {

		var sessionid;

		before(function(done) {

			var loginData = {
				'email' : 'sm@jamb2b.com',
				'pword' : 'smsmsm'
			};

			request(app)

				.post('/login')
				.send(loginData)
				// end handles the response
				.end(function(err, res) {

					if (err) {
						throw err;
					}

					else {
						sessionid = res.body.data.sessionid;
					}
					done();
				});
		});


		it('with existing user should return ERROR: user already exists', function(done) {

			var userData = {
				'email' : 'sm@jamb2b.com',
				'firstname' : 'Sudeep',
				'lastname' : 'Mathur'
			};

			request(app)

				.post('/admin/users?sessionid='+sessionid)
				.send(userData)
				.end(function(err, res2) {
					if (err) {
						console.log('post error!!');
						throw err;
					}

//					console.log(res2.body);
					res2.body.code.should.equal(222);
					res2.body.data.should.equal('Email already exists');
					done();
				});
		});

	});




	describe('with valid session and user first name and last name', function() {

		var sessionid;

		before(function(done) {

			var loginData = {
				'email' : 'sm@jamb2b.com',
				'pword' : 'smsmsm'
			};

			request(app)

				.post('/login')
				.send(loginData)
				// end handles the response
				.end(function(err, res) {
					if (err) {
						throw err;
					}

					else {
						sessionid = res.body.data.sessionid;
					}
					done();
				});
		});


		after(function(done) {

			var deleteUserData = {
				'email' : 'domokennaressa@gmail.com'
			};

			request(app)
				.delete('/admin/users?sessionid='+sessionid)
				.send(deleteUserData)
				// end handles the response
				.end(function(err, res) {
					if (err) {
						console.log('user delete error!!');
						throw err;
					}
					else {
						console.log(res.body);
					}
					done();
				});
		});



		it('with new user should return user added confirmation', function(done) {


			var userData = {
				'email' : 'domokennaressa@gmail.com',
				'firstname' : 'DrThisIsFrom',
				'lastname' : 'MochaJunkhead'
			};

			request(app)

				.post('/admin/users?sessionid='+sessionid)
				.send(userData)
				.end(function(err, res2) {
					if (err) {
						console.log('post error!!');
						throw err;
					}

//								console.log(res2.body);
					res2.body.code.should.equal(1);
					res2.body.data.should.equal('User added');
					done();
				});
		});




	});


});

