var fs = require('fs');
var https = require('https');
var privateKey  = fs.readFileSync('sslcert/server.key', 'utf8');


var certificate, credentials, file, files, ca;

if (process.env.NODE_ENV === 'production') {

	files = ['COMODORSAAddTrustCA.crt', 'COMODORSADomainValidationSecureServerCA.crt', 'AddTrustExternalCARoot.crt'];

	certificate = fs.readFileSync('sslcert/jamb2b_com.crt', 'utf8');

	ca = (function() {
		var _i, _len, _results;

		_results = [];
		for (_i = 0, _len = files.length; _i < _len; _i++) {
			file = files[_i];
			_results.push(fs.readFileSync("sslcert/" + file));
		}
		return _results;
	})();

	credentials = {ca: ca, key: privateKey, cert: certificate};

}


else {
	certificate = fs.readFileSync('sslcert/server.crt', 'utf8');
	credentials = {key: privateKey, cert: certificate};
}



var express = require('express'),
  b2b = require('./routes/b2b');
  // path = require('path');

var app = express();

module.exports = app;

var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
};


app.configure(function () {
  app.use(express.logger('dev'));     /* 'default', 'short', 'tiny', 'dev' */
  app.use(express.bodyParser());
  app.use(allowCrossDomain);
  app.use(express.methodOverride());
  app.use(express.json());
  app.use(express.urlencoded());
  // app.use(express.bodyParser({keepExtensions:true,uploadDir:path.join(__dirname,'/public/uploaded_images')}));
});


// app.use("/styles", express.static(__dirname + '/public/styles'));

// app.use("/images", express.static(__dirname + '/public/images'));

// app.get('/', function(req, res) {
//   res.sendfile('public/index.html')
// });

// app.get('/survey', function(req, res) {
//   res.redirect('https://docs.google.com/forms/d/1OB9YkNpGxokYQoN04hbvqUIZuiFT-EDU3UKpKt_DIMM/viewform')
// });

app.post('/login', b2b.userLogin);
app.get('/users', b2b.usersFindAll);
app.get('/users/:userId', b2b.usersFindById);
app.post('/users', b2b.userAdd);
app.post('/admin/users', b2b.adminUserAdd);
app.put('/users/:id', b2b.usersUpdate);
app.get('/companies', b2b.companiesFindAll);
app.get('/companies/:companyId', b2b.companyFindById);
app.post('/companies', b2b.companiesAdd);
app.put('/companies/:id', b2b.companyUpdate);
app.post('/companies/:companyId/review', b2b.companyInsertReview);
app.get('/companies/:id/products', b2b.productFindByCompanyId);
app.get('/products', b2b.productsFindAll);
app.get('/products/:productId', b2b.productFindById);
app.post('/products', b2b.productAdd);
app.put('/products/:id', b2b.productUpdate);
app.get('/reviews', b2b.reviewsFindAll);
app.post('/search', b2b.searchCompaniesProducts);
app.post('/forgot', b2b.forgotPasswordSendReset);
app.post('/forgot/:resetHash', b2b.checkPasswordResetHashValid);
app.post('/reset', b2b.resetPassword);
app.post('/upload', b2b.uploadPhoto);
app.get('/session', b2b.checkSession);
app.delete('/session', b2b.deleteSession);

if (process.env.NODE_ENV==='local') {
	app.delete('/admin/users', b2b.userDelete);
}

var httpsServer = https.createServer(credentials, app);
httpsServer.listen(8445);

// app.listen(8080);
console.log('Listening on port 8445...');