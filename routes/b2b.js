var mongo = require('mongodb'),
	bcrypt = require('bcrypt'),
	sha256 = require('sha256'),
	validator = require('../bower_components/validator-js/validator.js'),
	fs = require('fs'),
	pg = require('pg'),
	dbInfo = require('../config/config.js'),
	entities = require('entities');

var dbData = {
	type: dbInfo.dbConfig.dbtype,
	user: dbInfo.dbConfig.dbuser,
	password: dbInfo.dbConfig.dbpassword,
	name: dbInfo.dbConfig.dbname,
	host: dbInfo.dbConfig.dbhost
};


var conString =  dbData.type + '://' + dbData.user + ':' + dbData.password + '@' + dbData.host + '/' + dbData.name;

var Server = mongo.Server,
	Db = mongo.Db,
	BSON = mongo.BSONPure;

var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new Db('b2bdb', server);

db.open(function(err, db) {
	if(!err) {
		console.log("Connected to 'b2bdb' database");
		db.collection('users', {strict:true}, function(err, collection) {
			if (err) {
				console.log("The 'users' collection doesn't exist. Creating it with sample data...");
				populateDB();
			}
		});
	}
});



// ************************************************
// ************************************************
//                ENVIRONMENT
// ************************************************
// ************************************************

var DomainServer = null;

if (process.env.NODE_ENV==='production') {
	DomainServer = 'https://www.jamb2b.com';
}

else if (process.env.NODE_ENV==='stage') {
	DomainServer = 'https://stage.jamb2b.com';
}

else if (process.env.NODE_ENV==='dev') {
	DomainServer = 'https://dev.jamb2b.com';
}

else if (process.env.NODE_ENV==='local') {
	DomainServer = 'https://127.0.0.1:8443';
}

else {
	DomainServer = 'https://dev.jamb2b.com';
}




// ************************************************
// ************************************************
//                SESSION
// ************************************************
// ************************************************


function isSessionValid (pSessionId, callback) {

	// clean session id
	var sessionid = validator.escape(pSessionId);

	// check session is not empty
	if (typeof sessionid === 'undefined' || sessionid === '' || sessionid === null) {
		console.log('no session id provided');
		callback(false);
		return;
	}

	// if session is in DB
	pg.connect(conString, function(err, client, done) {
		if(err) {
			console.log(err);
			console.log('db connection error');
			callback(false);
			return;
		}
//		console.log('session query happening');
		client.query('SELECT sessionid, sessiontimestamp from users where sessionid = \'' + sessionid + '\'', function(err, result) {
			//call `done()` to release the client back to the pool
			done();

//			console.log(result.rowCount);
			if(err) {
				console.log(err);
				callback(false);
				return;
			}

			if (result.rowCount===0) {
				console.log('no results returned');
				callback(false);
				return;
			}

			if (result.rowCount>1) {
				console.log('error: more than one record returned for session id');
				callback(false);
				return;
			}

			if (result.rowCount===1) {
//				console.log('session validated');
				callback(true);
			}

			else {
				console.log('some other error');
				callback(false);
			}
		});
	});

}



exports.checkSession = function(req, res) {

	var sessionid = validator.escape(req.query.sessionid);

	function sendSessionCheckResultToClient(result) {
		console.log(result);
		if (result === true) {
			res.send( { "code" : 1, "data" : 'session valid'});
		}
		else {
			res.send( { "code" : 900, "data" : 'session invalid'});
		}
	}

	isSessionValid(sessionid, sendSessionCheckResultToClient);

};




// ************************************************
// ************************************************
//                USER
// ************************************************
// ************************************************


exports.userLogin = function(req, res) {

	// escape inputs
	var inputEmail = validator.escape(req.body.email);
	var inputPassword = validator.escape(req.body.pword);

	// validate inputs are not empty
	if (isEmptyOrNull(inputEmail) || isEmptyOrNull(inputPassword)) {
		res.send({"code" : 0, "error" : "Missing username or password"});
		return;
	}

	// validate email is in email format
	if (!validator.isEmail(inputEmail)) {
		res.send({"code" : 2, "error" : "invalid email"});
		return;
	}

	// validate password meets password rules
	var passwordRegEx = /^.{6,15}$/;

	if (!passwordRegEx.test(inputPassword)) {
		res.send({"code" : 3, "error" : "invalid password"});
		return;
	}


	// if ((typeof req.body.email === "undefined") || (typeof req.body.pword === "undefined")) {
	//   res.send({"code" : 0, "error" : "request missing username or password"});
	// }

	pg.connect(conString, function(err, client, done) {
		if(err) {
			res.send( { "code" : 0, "data" : 'pool error'});
			return console.error('error fetching client from pool', err);
		}
		var qu = 'select password, firstname, lastname, user_type, password_temp from users where email = \'' + inputEmail + '\'';
//		console.log(qu);
		client.query(qu, function(err, result) {
			//call `done()` to release the client back to the pool

//			console.log(result);

			if(err) {
				res.send( { "code" : 0, "data" : 'query error'});
				return console.error('error running query', err);
			}

			// user not found
			else if (result.rowCount === 0) {
				res.send({'code' : 3, 'error' : 'user not found'});
				return;
			}

			var dbpass = result.rows[0].password;
			var dbfirstname = result.rows[0].firstname;
			var dblastname = result.rows[0].lastname;

//			console.log(dbpass + ' ' + dbfirstname + ' ' + dblastname);


			bcrypt.compare(inputPassword, dbpass, function(err2, resp) {

				// login successful
				if (resp === true) {

//					console.log('PASSWORD MATCHES!!');

					// before valid session is created
					// check if user has a temporary password
					console.log('PASSWORD TEMP: vv');
					console.log(result.rows[0].password_temp);

					if (result.rows[0].password_temp === 1) {
						res.send({ 'code' : 555, 'data' : 'create new password' });
						return;
					}


					// create session id
					var sessionid = sha256(Math.random() * Math.random());

//					console.log('sessionid: ' + sessionid);


					// store session in db
//					collection.update( { _id : hashItems._id }, { $set : { 'sessionid' : sessionid, 'sessionTimestamp' : new Date().getTime()/1000 } }, function(err3, itemsCount) {
					qu = 'update users set sessionid = \'' + sessionid + '\', sessionTimestamp = \'' + new Date().getTime() + '\' where email = \'' + inputEmail + '\'';
//					console.log(qu);
					client.query(qu, function(err3, result2) {

						done();

//						console.log(result2);

						if (err3) {
							console.log(err3);
							res.send({ 'code' : 0, 'data' : 'error' });
						}
						else {

							// send session id back
							res.send({ 'code' : 1, 'data' : { 'sessionid' : sessionid, 'firstname': dbfirstname, 'lastname' : dblastname}});
						}
					});
				}
				else {
//					console.log('password mismatch');
					res.send({ 'code' : 2, 'data' : 'failed' });
				}
			});

		});
	});
};




exports.usersFindById = function(req, res) {

//	var sessionid = validator.escape(req.query.sessionid);

	function sendSessionAndUserResultToClient() { // sResult

		// session valid
//		if (sResult === true) {

		pg.connect(conString, function(err, client, done) {

			if(err) {
				res.send( { "code" : 3, "data" : 'pool error'});
				return console.error('error fetching client from pool', err);
			}

			else {

				var userid = validator.escape(req.params.userId);

				client.query('SELECT email, firstname, lastname, username, company_id, title from users where id = ' + userid, function(err, result) {
					//call `done()` to release the client back to the pool

					console.log();
					if(err) {
						res.send( { "code" : 0, "data" : 'query error'});
						return console.error('error running query', err);
					}

					if (result.rowCount===0) {
						res.send({'code' : 2, 'data' : 'no users found'});
					}

					else if (result.rowCount===1) {

						var objToReturn = {};
						objToReturn.user = result.rows[0];

						client.query('select reviews.subject, reviews.text, reviews.timestamp, companies.name as company_name, ' +
							'companies.id as company_id, products.name as product_name, products.id as product_id ' +
							'from reviews ' +
							'left join companies on reviews.target_companyid = companies.id ' +
							'left join products on reviews.target_productid = products.id ' +
							'where reviews.from_userid = ' + userid, function(err2, result2) {
							done();

							if(err2) {
								res.send( { "code" : 1, "data" : objToReturn});
								return console.error('user message query error');
							}

							console.log(result2);
							objToReturn.reviews = result2.rows;
							res.send( { "code" : 1, "data" : objToReturn});

						});
					}

					// error - more than one user found for some reason
					else {
						res.send( { "code" : 4, "data" : 'error > 1 users returned'});
					}

				});
			}
		});

//		}

		// session not valid
//		else {
//			res.send( { "code" : 900, "data" : 'session invalid'});
//		}

	}

	sendSessionAndUserResultToClient();
//	isSessionValid(sessionid, sendSessionAndUserResultToClient);


};


// postgres query
exports.usersFindAll = function(req, res) {

	var sessionid = validator.escape(req.query.sessionid);

	function sendSessionAndUserResultToClient(sResult) {

		// session is valid
		if (sResult===true) {
			pg.connect(conString, function(err, client, done) {
				if(err) {
					res.send( { "code" : 3, "data" : 'pool error'});
					return console.error('error fetching client from pool', err);
				}
				client.query('SELECT id, email, firstname, lastname, username, company_id from users', function(err, result) {
					//call `done()` to release the client back to the pool
					done();

					if(err) {
						res.send( { "code" : 0, "data" : 'query error'});
						return console.error('error running query', err);
					}
					else if (result.rowCount===0) {
						res.send({'code' : 2, 'data' : 'no users found'});
					}
					else if (result.rowCount>0) {
						res.send( { "code" : 1, "data" : result.rows});
					}
					else {
						res.send( { "code" : 3, "data" : 'error'});
					}
				});
			});
		}

		// session is not valid
		else {
			res.send( { "code" : 900, "data" : 'session expired'});
		}
	}

	isSessionValid(sessionid, sendSessionAndUserResultToClient);

};



//exports.userAdd = function(req, res) {
//
//  // escape inputs
//  var inputEmail = validator.escape(req.body.email);
//  var inputPassword = validator.escape(req.body.pword);
//
//  // validate inputs are not empty
//  if (isEmptyOrNull(inputEmail) || isEmptyOrNull(inputPassword)) {
//    res.send({"code" : 0, "error" : "missing username or password"});
//    return;
//  }
//
//  // validate email is in email format
//  if (!validator.isEmail(inputEmail)) {
//    res.send({"code" : 2, "error" : "invalid email"});
//    return;
//  }
//
//  // validate password meets password rules
//  var passwordRegEx = /^.{6,15}$/;
//
//  if (!passwordRegEx.test(inputPassword)) {
//    res.send({"code" : 3, "error" : "invalid password"});
//    return;
//  }
//
//
//  // check if user already exists
//  db.collection('users', function(userErr, collection) {
//    collection.findOne({ 'email' : inputEmail}, {'email' : 1},
//      function(toArrErr, items) {
//        if (!toArrErr) {
//
//          // email already exists
//          if (!(items === null)) {
//            console.log('email already exists');
//            res.send( { 'code' : 4, 'data' : 'email already exists' });
//          }
//
//          // email does not already exist
//          else {
//
//            bcrypt.hash(inputPassword, 8, function(err, hash) {
//
//              console.log(inputPassword);
//              console.log(hash);
//
//              db.collection('users', function(err, collection) {
//                collection.insert({'email' : inputEmail, 'password' : hash}, {'continueOnError' : false, 'safe' : true },  function(err, docInserted){
//                    if (err) {
//                      res.send( { 'code' : 5, 'error' : err.name } );
//                    }
//                    else {
//                      sendmailRegisterEmailVerify(inputEmail);
//                      res.send({'code' : 1, 'data' : 'user added'});
//                    }
//                  }
//                );
//              });
//
//            });
//          }
//
//        }
//      }
//    );
//  });
//
//};



exports.userAdd = function(req, res) {

	// escape inputs
	var inputEmail = validator.escape(req.body.email);
	var inputPassword = validator.escape(req.body.pword);

	// validate inputs are not empty
	if (isEmptyOrNull(inputEmail) || isEmptyOrNull(inputPassword)) {
		res.send({"code" : 0, "error" : "missing username or password"});
		return;
	}

	// validate email is in email format
	if (!validator.isEmail(inputEmail)) {
		res.send({"code" : 2, "error" : "invalid email"});
		return;
	}

	// validate password meets password rules
	var passwordRegEx = /^.{6,15}$/;

	if (!passwordRegEx.test(inputPassword)) {
		res.send({"code" : 3, "error" : "invalid password"});
		return;
	}


	// check if user already exists
	db.collection('users', function(userErr, collection) {
		collection.findOne({ 'email' : inputEmail}, {'email' : 1},
			function(toArrErr, items) {
				if (!toArrErr) {

					// email already exists
					if (!(items === null)) {
						console.log('email already exists');
						res.send( { 'code' : 4, 'data' : 'email already exists' });
					}

					// email does not already exist
					else {
						console.log('email does not already exist!!');
						bcrypt.hash(inputPassword, 8, function(err, hash) {

							console.log(inputPassword);
							console.log(hash);

							db.collection('users', function(err, collection) {
								collection.insert({'email' : inputEmail, 'password' : hash}, {'continueOnError' : false, 'safe' : true },  function(err, docInserted){
										if (err) {
											res.send( { 'code' : 5, 'error' : err.name } );
										}
										else {
											sendmailRegisterEmailVerify(inputEmail);
											res.send({'code' : 1, 'data' : 'user added'});
										}
									}
								);
							});

						});
					}

				}
			}
		);
	});

};



function generateRandomPassword(callback) {

	var newPass = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for( var i=0; i < 10; i++ ) {
		newPass += possible.charAt(Math.floor(Math.random() * possible.length));
	}

	callback(newPass);
}






exports.adminUserAdd = function(req, res) {

	var sessionid = validator.escape(req.query.sessionid);
	console.log('server - passed session id : '+ sessionid);

	function sendSessionAndUserResultToClient(sResult) {

		// session is valid
		if (sResult===true) {
			console.log('session is valid!!');

			pg.connect(conString, function(err, client, done) {
				if(err) {
					res.send( { "code" : 3, "data" : 'pool error'});
					return console.error('error fetching client from pool', err);
				}


				// escape inputs
				var inputEmail = validator.escape(req.body.email);
				var inputFirstname = validator.escape(req.body.firstname);
				var inputLastname = validator.escape(req.body.lastname);

				// validate inputs are not empty
				if (isEmptyOrNull(inputEmail) || isEmptyOrNull(inputFirstname) || isEmptyOrNull(inputLastname)) {
					console.log('inputs are empty!!');
					res.send({"code" : 0, "error" : "missing email or name"});
					return;
				}

				// validate email is in email format
				if (!validator.isEmail(inputEmail)) {
					console.log('email is invalid!!');
					res.send({"code" : 2, "error" : "invalid email"});
					return;
				}

				// validate password meets password rules
				var nameRegEx = /^[a-zA-Z\ -]{1,50}$/;

				if (!nameRegEx.test(inputFirstname) || !nameRegEx.test(inputLastname)) {
					console.log('name is not valid!!');
					res.send({"code" : 3, "error" : "invalid name"});
					return;
				}

				// check if user already exists
				client.query('SELECT email from users where email = \'' + inputEmail + '\'', function(err, result) {

					if(err) {
						res.send( { "code" : 7, "error" : 'existing email query error'});
						console.log('error running query', err);
						done();
					}

					// email already exists
					else if (result.rowCount > 0) {
						console.log('email already exists!');
						console.log(result);
						res.send({'code' : 222, 'data' : 'Email already exists'});
						done();
					}

					// email does not already exist
					else if (result.rowCount === 0) {

						generateRandomPassword(function(tempPassword) {

							console.log(tempPassword);

							bcrypt.hash(tempPassword, 8, function(err, hash) {

								console.log(tempPassword);
								console.log(hash);

								var inputString = [inputEmail, inputFirstname, inputLastname, hash, 1];
								var insertQuery = 'insert into users (email, firstname, lastname, password, password_temp) values (\'' + inputString.join("','") + '\')';

								console.log(insertQuery);

								client.query(insertQuery, function(err2, result2) {

									if (err2) {
										console.log(err2);
										res.send({'code' : 9, 'error' : 'ERROR'});
										return;
									}

									else {
										var loginLink = DomainServer + '/#/login';
										console.log('USER ADDED');
										res.send({'code' : 1, 'data' : 'User added'});
										sendmailAdminUserAdd(inputEmail, inputFirstname, loginLink, tempPassword);
									}

								});
							});
						});

					}
				});
			});
		}

		// session is not valid
		else {
			console.log('session is not valid');
			res.send( { "code" : 900, "error" : 'session expired'});
		}
	}

	isSessionValid(sessionid, sendSessionAndUserResultToClient);

};






var sendmailAdminUserAdd = function(inputEmail, inputFirstname, loginLink, tempPassword) {

	console.log('send user email admin add called');
	var mandrill = require('mandrill-api/mandrill');
	var mandrill_client = new mandrill.Mandrill('-vEpRQHnZFM67lLBkU0HfQ');


	var template_name = "user-added-by-admin";

	var template_content = [
		{
			"name" : "USEREMAIL",
			"content" : inputEmail
		},
		{
			"name" : "USERFIRSTNAME",
			"content" : inputFirstname
		},
		{
			"name" : "TEMPPASSWORD",
			"content" : tempPassword
		},
		{
			"name" : "LOGINLINK",
			"content" : loginLink
		}

	];


	var message = {
		"subject": "Your JamB2B account has been created",
		"from_email": "robot@jamb2b.com",
		"from_name": "JamB2B",
		"to": [
			{
				"email": inputEmail,
				"name": null,
				"type": "to"
			}
		],
		"headers": {
			"Reply-To": null
		},
		"important": false,
		"track_opens": false,
		"track_clicks": true,
		"auto_text": null,
		"auto_html": null,
		"inline_css": null,
		"url_strip_qs": null,
		"preserve_recipients": null,
		"view_content_link": null,
		"bcc_address": "accounts@jamb2b.com",
		"tracking_domain": null,
		"signing_domain": null,
		"return_path_domain": null,
		"merge": true,
		"global_merge_vars": [
			{
				"name": "USEREMAIL",
				"content": "error"
			}
		],
		"merge_vars": [
			{
				"rcpt": inputEmail,
				"vars": [
					{
						"name" : "USEREMAIL",
						"content" : inputEmail
					},
					{
						"name" : "USERFIRSTNAME",
						"content" : inputFirstname
					},
					{
						"name" : "TEMPPASSWORD",
						"content" : tempPassword
					},
					{
						"name" : "LOGINLINK",
						"content" : loginLink
					}
				]
			}
		],
		"tags": [
			"email_confirm", "new_user", "registration"
		]
	};


	var async = false;
	// var ip_pool = "Main Pool";
	// var send_at = "example send_at";

	mandrill_client.messages.sendTemplate(
		{
			"template_name": template_name,
			"template_content": template_content,
			"message": message,
			"async": async
			// "ip_pool": ip_pool,
			// "send_at": send_at
		},
		function(result) {
			console.log(result);
			/*
			 [{
			 "email": "recipient.email@example.com",
			 "status": "sent",
			 "reject_reason": "hard-bounce",
			 "_id": "abc123abc123abc123abc123abc123"
			 }]
			 */
		},
		function(e) {
			// Mandrill returns the error as an object with name and message keys
			console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
			// A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		}
	);

};







var createInsertPasswordResetHash = function() {

	var newHash = sha256(Math.random() * Math.random());

	db.collection('users', function(userErr, collection) {

		collection.update( { _id : hashItems._id }, { $set : { 'sessionid' : sessionid } }, function(err3, itemsCount) {
		});
	});

};


// var doesEmailExist = function(inputEmail) {

//   console.log('var doesEmailExist called with : ' + inputEmail);


//   // check if user already exists
//   db.collection('users', function(err, collection) {
//     if (!err) {

//       collection.findOne({ 'email' : inputEmail}, {'email' : 1}, function(err2, items) {
//           if (!err2) {

//             console.log(items);


//             if (items === null) {
//               console.log('query response is null, returning false');
//               return false;
//             }
//             else {
//               console.log('query response is not null, returning true');
//               return true;
//             }
//           }
//           else {
//             console.log('error in query, returning true');
//             return true;
//           }
//         }
//       );

//     }

//     else {
//       console.log(err);
//       return true;
//     }

//   });

// };



exports.forgotPasswordSendReset = function(req, res) {

	// run validation on email
	var inputEmail = validator.escape(req.body.email);

	if (!validator.isEmail(inputEmail)) {
		res.send( { 'code' : 0, 'error' : 'not a valid email address' } );
		return;
	}

	// check if email address exists in the system
	db.collection('users', function(userErr, collection) {

		collection.findOne({ 'email' : inputEmail}, {'email' : 1, _id:1},

			function(noEmailErr, queryResult) {

				// if email exists error
				if (noEmailErr) {
					res.send( { 'code' : 0, 'error' : noEmailErr} );
					return;
				}

				// no error with find email
				else {

					// email does not exist
					if (queryResult === null) {
						console.log('email does not exist');
						res.send( { 'code' : 2, 'data' : 'email does not exist' });
						return;
					}

					// email exists
					else {

						// generate hash
						var passResetHashCode = sha256(Math.random() * Math.random());

						// inset hash and timestamp
						collection.update( { '_id': queryResult._id }, { $set : { 'resetCode' : passResetHashCode, 'resetCodeCreateTimestamp' : 'time.now' }}, function(err, updateResult) {

							if (err) {
								console.log(err);
								res.send( { 'code' : 3, 'data' : 'error' });
								return;
							}

							else {
								console.log(updateResult);
								sendmailResetPassword(inputEmail, passResetHashCode);
								res.send( { 'code' : 1, 'data' : 'email sent' });
							}

						});


					}


				}
			}
		);
	});



};



exports.checkPasswordResetHashValid = function(req, res) {

	// run validation on email
	var escHash = validator.escape(req.params.resetHash);
	console.log(escHash);


	// check if email address exists in the system
	db.collection('users', function(userErr, collection) {

		collection.findOne({ 'resetCode' : escHash}, {_id:1},

			function(checkHashErr, queryResult) {

				// if error
				if (checkHashErr) {

					console.log(checkHashErr);
					res.send( { 'code' : 0, 'data' : 'error'} );
					return;
				}

				// TODO: check if existing hash is still valid (generated within a time frame)

				// if hash exists
				else if (queryResult != null) {

					console.log(queryResult);
					res.send( { 'code' : 1, 'data' : queryResult } );
					return;
				}

				// hash doesn't exit
				else  {
					res.send( { 'code' : 2, 'data' : 'request does not exist' } );
					return;
				}
			});

	});

};




exports.resetPassword = function(req, res) {

	// sanitize inputs
	var escHash = validator.escape(req.body.resetCode);
	console.log('escHash' + escHash);

	// check password is of the correct format
	var passwordRegEx = /^.{6,15}$/;
	var sanPassword1 = validator.escape(req.body.newPassword);

	if (!passwordRegEx.test(sanPassword1)) {
		console.log('password of invalid format');
		res.send({"code" : 0, "error" : "invalid password format"});
		return;
	}

	// check hash is still valid

	db.collection('users', function(userErr, collection) {

		collection.findOne({ 'resetCode' : escHash}, {_id:1},

			function(checkHashErr, queryResult) {

				// if error
				if (checkHashErr) {

					console.log(checkHashErr);
					res.send( { 'code' : 0, 'data' : 'error'} );
					return;
				}

				// TODO: check if existing hash is still valid (generated within a time frame)

				// reset code not available
				else if (queryResult === null) {

					res.send( { 'code' : 2, 'data' : 'Reset code not found'} );
					return;
				}

				// reset code is valid
				else if (queryResult != null) {

					bcrypt.hash(sanPassword1, 8, function(err, hash) {

						collection.update( { 'resetCode' : escHash } , { $set : { 'password' : hash} }, { 'safe' : true}, function(err, result) {

							if (err) {
								console.log(err);
								res.send( { 'code' : 3, 'data' : 'error'} );
								return;
							}

							else {
								console.log(result);
								res.send( { 'code' : 1, 'data' : result } );
							}

						});
					});
				}


				// update database with new password

				// send back success

			}
		);
	});
};




exports.uploadPhoto = function(req,res) {

	console.log(req.files.file);

	fs.readFile(req.files.file.path, function (err, data) {





		// 5MB limit on uploads
		if (req.files.file.size > 5000000) {
			console.log('file is too large');
			res.send({'code' : 2, 'data' : 'file exceeds size limit'});
			return false;
		}


		var newPath = __dirname + "/../public/uploaded_images/" + req.files.file.name;

		fs.writeFile(newPath, data, function (err) {

			res.send({'code' : 1, 'data' : 'image uploaded'});
			// res.redirect("back");
		});



	});

};



var isEmptyOrNull = function(inputString) {
	if ( (typeof inputString === 'undefined') || (inputString === null) || (inputString === '') ) {
		return true;
	}
	else {
		return false;
	}
};





var isEmailValid = function(inputEmail) {

	var emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	return emailRegEx.test(inputEmail);

};



var sendmailRegisterEmailVerify = function(inputEmail) {


	var mandrill = require('mandrill-api/mandrill');
	var mandrill_client = new mandrill.Mandrill('-vEpRQHnZFM67lLBkU0HfQ');


	var template_name = "new-user-email-address-confirmation";

	var template_content = [
		{
			"name":"VERIFYEMAILLINK",
			"content": DomainServer + '/TEMPLATE_CONTENT'
		},
		{
			"name" : "EMAILTOVERIFY",
			"content" : inputEmail
		}
	];


	var message = {
		"subject": "Welcome! Please verify your email address",
		"from_email": "robot@jamb2b.com",
		"from_name": "JamB2B",
		"to": [
			{
				"email": inputEmail,
				"name": null,
				"type": "to"
			}
		],
		"headers": {
			"Reply-To": null
		},
		"important": false,
		"track_opens": false,
		"track_clicks": true,
		"auto_text": null,
		"auto_html": null,
		"inline_css": null,
		"url_strip_qs": null,
		"preserve_recipients": null,
		"view_content_link": null,
		"bcc_address": "accounts@jamb2b.com",
		"tracking_domain": null,
		"signing_domain": null,
		"return_path_domain": null,
		"merge": true,
		"global_merge_vars": [
			{
				"name": "VERIFYEMAILLINK",
				"content": "error"
			},
			{
				"name" : "EMAILTOVERIFY",
				"content" : "error"
			}
		],
		"merge_vars": [
			{
				"rcpt": inputEmail,
				"vars": [
					{
						"name": "VERIFYEMAILLINK",
						"content": DomainServer + "/verify/MERGE_VARS/?var=asdas34fs4324234asd"
					},
					{
						"name" : "EMAILTOVERIFY",
						"content" : inputEmail
					}
				]
			}
		],
		"tags": [
			"email_confirm", "new_user", "registration"
		]
	};


	var async = false;
	// var ip_pool = "Main Pool";
	// var send_at = "example send_at";

	mandrill_client.messages.sendTemplate(
		{
			"template_name": template_name,
			"template_content": template_content,
			"message": message,
			"async": async
			// "ip_pool": ip_pool,
			// "send_at": send_at
		},
		function(result) {
			console.log(result);
			/*
			 [{
			 "email": "recipient.email@example.com",
			 "status": "sent",
			 "reject_reason": "hard-bounce",
			 "_id": "abc123abc123abc123abc123abc123"
			 }]
			 */
		},
		function(e) {
			// Mandrill returns the error as an object with name and message keys
			console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
			// A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		}
	);

};



var sendmailResetPassword = function(inputEmail, inputHashCode) {

	console.log('sendmail Reset Password called with ' + inputEmail + ' and ' + inputHashCode);
	// return;

	var resetLink = DomainServer + '/#/reset/' + inputHashCode;

	var mandrill = require('mandrill-api/mandrill');
	var mandrill_client = new mandrill.Mandrill('-vEpRQHnZFM67lLBkU0HfQ');


	var template_name = "reset-password";

	var template_content = [
		{
			"name":"RESETPASSWORDLINK",
			"content": resetLink
		}
	];


	var message = {
		"subject": "Your password reset request",
		"from_email": "robot@jamb2b.com",
		"from_name": "JamB2B",
		"to": [
			{
				"email": inputEmail,
				"name": null,
				"type": "to"
			}
		],
		"headers": {
			"Reply-To": null
		},
		"important": false,
		"track_opens": false,
		"track_clicks": true,
		"auto_text": null,
		"auto_html": null,
		"inline_css": null,
		"url_strip_qs": null,
		"preserve_recipients": null,
		"view_content_link": null,
		"bcc_address": "accounts@jamb2b.com",
		"tracking_domain": null,
		"signing_domain": null,
		"return_path_domain": null,
		"merge": true,
		"global_merge_vars": [
			{
				"name": "RESETPASSWORDLINK",
				"content": "error"
			}
		],
		"merge_vars": [
			{
				"rcpt": inputEmail,
				"vars": [
					{
						"name": "RESETPASSWORDLINK",
						"content": resetLink
					}
				]
			}
		],
		"tags": [
			"email_confirm", "new_user", "registration"
		]
	};


	var async = false;
	// var ip_pool = "Main Pool";
	// var send_at = "example send_at";

	mandrill_client.messages.sendTemplate(
		{
			"template_name": template_name,
			"template_content": template_content,
			"message": message,
			"async": async
			// "ip_pool": ip_pool,
			// "send_at": send_at
		},
		function(result) {
			console.log(result);
			/*
			 [{
			 "email": "recipient.email@example.com",
			 "status": "sent",
			 "reject_reason": "hard-bounce",
			 "_id": "abc123abc123abc123abc123abc123"
			 }]
			 */
		},
		function(e) {
			// Mandrill returns the error as an object with name and message keys
			console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
			// A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		}
	);

};




exports.usersUpdate = function(req, res) {
	var id = req.params.id;
	var user = req.body;
	console.log('Updating user: ' + id);
	console.log(JSON.stringify(user));
	db.collection('users', function(err, collection) {
		collection.update({'_id':new BSON.ObjectID(id)}, user, {safe:true}, function(err, result) {
			if (err) {
				console.log('Error updating user: ' + err);
				res.send({'error':'An error has occurred'});
			} else {
				console.log('' + result + ' document(s) updated');
				res.send(user);
			}
		});
	});
};


exports.deleteSession = function(req, res) {

	var escSessionId = validator.escape(req.body.sessionId);


	pg.connect(conString, function(err, client, done) {
		if(err) {
			res.send( { "code" : 0, "data" : 'pool error'});
			return console.error('error fetching client from pool', err);
		}

		var qu = 'update users set sessionid = null, sessiontimestamp = null where sessionid = \'' + escSessionId + '\'';

		client.query(qu, function(err, result) {
			//call `done()` to release the client back to the pool
			done();

			console.log(result);
			console.log(err);

			if(err) {
				res.send( { "code" : 0, "data" : 'query error'});
				return console.error('error running query', err);
			}

			if (result.rowCount === 0) {
				res.send( { "code" : 1, "data" : 'no session'});
			}

			else if (result.rowCount===1) {
				res.send( { "code" : 1, "data" : 'session deleted'});
			}

		});
	});



};


exports.userDelete = function(req, res) {

	console.log('user delete called');

	var sessionid = validator.escape(req.query.sessionid);

	function sendSessionAndMethodResultToClient(sResult) {

		// session is valid
		if (sResult === true) {

			var emailAddress = req.body.email;

			pg.connect(conString, function(err, client, done) {
				if(err) {
					res.send( { "code" : 3, "data" : 'pool error'});
					return console.error('error fetching client from pool', err);
				}


				var queryString = 'delete from users where email = \'' + emailAddress + '\'';

				console.log(queryString);

				client.query(queryString, function(err, result) {
					//call `done()` to release the client back to the pool
					done();

					if(err) {
						res.send( { "code" : 0, "data" : 'query error'});
						return console.error('error running query', err);
					}

					else {
						if(result.rowCount===1){
							res.send({'code' : 1, 'data' : 'user deleted'});
						}
						else if (result.rowCount===0) {
							res.send({'code' : 1, 'data' : 'user to delete not found'});
						}
						else {
							res.send({'code' : 111, 'data' : 'user deletion error'});
						}
					}

				});
			});
		}

		// session is not valid
		else {
			res.send( { "code" : 900, "data" : 'session invalid'});
		}

	}

	isSessionValid(sessionid, sendSessionAndMethodResultToClient);
};



// ************************************************
// ************************************************
//            COMPANY
// ************************************************
// ************************************************


exports.companyFindById = function(req, res) {


//	var sessionid = validator.escape(req.query.sessionid);

	function sendSessionAndMethodResultToClient() {

		// session is valid
//		if (sResult === true) {

		var companyId = req.params.companyId;

		pg.connect(conString, function(err, client, done) {
			if(err) {
				res.send( { "code" : 3, "data" : 'pool error'});
				return console.error('error fetching client from pool', err);
			}
			client.query('SELECT name, id from companies where id = \'' + companyId + '\'', function(err, result) {
				//call `done()` to release the client back to the pool


				if(err) {
					res.send( { "code" : 0, "data" : 'query error'});
					return console.error('error running query', err);
				}

				if (result.rowCount===0) {
					res.send({'code' : 2, 'data' : 'no companies found'});
				}

				if (result.rowCount===1) {

					var objToReturn = {};
					objToReturn.company = result.rows[0];

					var queryStr =
						'select reviews.subject, reviews.text, reviews.rating, reviews.positive_review, reviews.timestamp, reviews.anonymous_type, ' +
							'reviews.target_companyid, users.firstname as user_firstname, users.lastname as user_lastname, users.id as user_id ' +
							'from reviews left join users on reviews.from_userid = users.id ' +
							'where reviews.target_companyid = ' + companyId +' and reviews.anonymous_type = 2 ' +
							'union ' +
							'select reviews.subject, reviews.text, reviews.rating, reviews.positive_review, reviews.timestamp, reviews.anonymous_type, ' +
							'reviews.target_companyid, null as user_firstname, null as user_lastname, null as user_id ' +
							'from reviews left join users on reviews.from_userid = users.id ' +
							'where reviews.target_companyid = ' + companyId + ' and (reviews.anonymous_type IS NULL or reviews.anonymous_type = 1)';


//					console.log(queryStr);

					client.query(queryStr, function(err2, result2) {

						if(err2) {
							res.send( { "code" : 1, "data" : objToReturn});
							return console.error('user message query error');
						}

						objToReturn.reviews = result2.rows;

						// re-encode review text
						for (var j=0; j<objToReturn.reviews.length; j++) {
							objToReturn.reviews[j].text = entities.decodeHTML(objToReturn.reviews[j].text);
//							console.log(objToReturn.reviews[j].text);
							objToReturn.reviews[j].subject = entities.decodeHTML(objToReturn.reviews[j].subject);
//							console.log(objToReturn.reviews[j].subject);
						}

						client.query('select name, description, id from products where company_id = ' + companyId, function(err3, result3) {
							done();


//					console.log(result3);
							objToReturn.products = result3.rows;
							res.send( { "code" : 1, "data" : objToReturn});

						});
					});
				}

				// error - more than one user found for some reason
				else {
					res.send( { "code" : 4, "data" : 'error > 1 users returned'});
				}

			});
		});

//		}
//
//		// session is not valid
//		else {
//			res.send( { "code" : 900, "data" : 'session invalid'});
//		}

	}

	sendSessionAndMethodResultToClient();

//	isSessionValid(sessionid, sendSessionAndMethodResultToClient);

};




exports.companiesFindAll = function(req, res) {

//	var sessionid = validator.escape(req.query.sessionid);

	function sendSessionAndMethodResultToClient() { // sResult

		// session is valid
//		if (sResult === true) {

		pg.connect(conString, function(err, client, done) {

			if(err) {
				res.send( { "code" : 3, "data" : 'pool error'});
				return console.error('error fetching client from pool', err);
			}

			client.query('SELECT id, name from companies', function(err2, result) {
				//call `done()` to release the client back to the pool
				done();

				if(err2) {
					res.send( { "code" : 0, "data" : 'query error'});
					return console.error('error running query', err2);
				}

				if (result.rowCount===0) {
					res.send({'code' : 2, 'data' : 'no users found'});
				}

				if (result.rowCount>0) {
					res.send( { "code" : 1, "data" : result.rows});
				}
			});
		});

//		}

		// session is not valid
//		else {
//			res.send( { "code" : 900, "data" : 'session invalid'});
//		}

	}

	sendSessionAndMethodResultToClient();

//	isSessionValid(sessionid, sendSessionAndMethodResultToClient);

};


exports.companiesAdd = function(req, res) {
	var company = req.body;
	console.log('Adding company: ' + JSON.stringify(company));
	db.collection('companies', function(err, collection) {
		collection.insert(company, {safe:true}, function(err, result) {
			if (err) {
				res.send({'error':'An error has occurred'});
			} else {
				console.log('Success: ' + JSON.stringify(result[0]));
				res.send(result[0]);
			}
		});
	});
};


exports.companyUpdate = function(req, res) {
	var id = req.params.id;
	var company = req.body;
	console.log(company);
	console.log('Updating company: ' + id);
	console.log(JSON.stringify(company));
	db.collection('companies', function(err, collection) {
		collection.update({'_id':new BSON.ObjectID(id)}, { $set : company }, function(err, result) {
			if (err) {
				console.log('Error updating company: ' + err);
				res.send({'error':'An error has occurred'});
			} else {
				console.log('' + result + ' document(s) updated');
				res.send(company);
			}
		});
	});
};


function addSlashes(str) {
	return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}


function safeQuoteForPostgres(str) {
	return (str + '').replace(/[']/g, '\'\'').replace(/\u0000/g, '\\0');
}



exports.companyInsertReview = function(req, res) {

	// check there is only one of each parameter


	// clean the parameters
	var sessionid = validator.escape(req.body.sessionid);
	var companyId = validator.escape(req.params.companyId);


	var dbAttributeNames = [];
	var dbAttributeValues = [];


	// input rating
	var starsRating = null;

	if (validator.escape(req.body.rating).trim().length > 0) {
		starsRating = validator.escape(req.body.rating);
		dbAttributeNames.push('rating');
		dbAttributeValues.push(starsRating);
	}


	// do business / recommend positive rating
	var positiveRating = null;

	if (validator.escape(req.body.positiveRating).trim().length > 0) {
		positiveRating = validator.escape(req.body.positiveRating);
		dbAttributeNames.push('positive_review');
		dbAttributeValues.push(positiveRating);
	}


	// written text review
	var writtenReviewTitle = null;
	var writtenReviewText = null;

	if (
		(validator.escape(req.body.writtenReviewTitle).trim().length > 0) &&
			(validator.escape(req.body.writtenReviewText).trim().length >0)
		) {
//		writtenReviewTitle = req.body.writtenReviewTitle;
//		writtenReviewText = req.body.writtenReviewText;
//		writtenReviewTitle = safeQuoteForPostgres(req.body.writtenReviewTitle);
//		writtenReviewText = safeQuoteForPostgres(req.body.writtenReviewText);
		writtenReviewTitle = entities.encodeHTML(req.body.writtenReviewTitle);
		writtenReviewText = entities.encodeHTML(req.body.writtenReviewText);
		dbAttributeNames.push('subject');
		dbAttributeNames.push('text');
		dbAttributeValues.push('\'' + writtenReviewTitle + '\'');
		dbAttributeValues.push('\'' + writtenReviewText + '\'');
	}


//	console.log(dbAttributeNames.join(','));
//	console.log(dbAttributeValues.join(','));


	// return if attributes are all empty
	if (dbAttributeNames.length ===0 || dbAttributeValues === 0) {
		res.send( { "code" : 10, "data" : 'data error'});
		return;
	}



	function sendSessionAndReviewInsertionConfirmationToClient(sResult) {

		// session is valid
		if (sResult === true) {

			pg.connect(conString, function(err, client, done) {

				// database connection error
				if(err) {
					res.send( { "code" : 3, "data" : 'pool error'});
					return console.error('error fetching client from pool', err);
				}


				// check if this session/user is able to write a review for this company
				// -- cannot review if they have already reviewed.  <<-- is this how it should be?
				// -- they should be able to modify an existing review

				// check if this user has left a review for this company before
				var queryUrl = 'select from_userid, subject, text, positive_review, rating from reviews where target_companyid = ' + companyId +' and from_userid = (select id from users where sessionid = \'' + sessionid + '\')';

				client.query(queryUrl, function(err, result) {

					done();

					// db connection error
					if (err) {
						res.send( { "code" : 0, "data" : 'connection error'});
						return console.error('connection error', err2);
					}

					// this user has left a review of this company before
					else if (result.rowCount > 0) {


						// write all present attributes into the DB

						// include the user id and company id
						dbAttributeNames.push('from_userid');
						dbAttributeValues.push(result.rows[0].from_userid);

						dbAttributeNames.push('target_companyid');
						dbAttributeValues.push(companyId);


						var query2 = 'insert into reviews (' + dbAttributeNames.join(',') + ') values (' + dbAttributeValues.join(',') + ');';

						console.log(query2);

						client.query(query2, function(err2, result2) {

							if (err2) {
								console.log(err2);
								res.send( { "code" : 200, "data" : 'error'});
							}

							else if (result2.rowCount === 1) {
								res.send( { "code" : 1, "data" : 'review added'});
							}

							else {
								res.send( { "code" : 0, "data" : 'error saving review'});
							}

						});

					}

					// user has not written a review of this company before
					else if (result.rowCount === 0) {

						// get user id
						var getUserIdQuery = 'select id from users where sessionid = \'' + sessionid + '\'';

						client.query(getUserIdQuery, function(err3, result3) {

							if (err3) {
								console.log(err3);
								res.send( { "code" : 200, "data" : 'error'});
							}

							// error where there are multiple sessions for this user
							else if (result3.rowCount !== 1) {
								res.send( { "code" : 0, "data" : 'error reading data'});
							}

							// correct response, there is only one session for this user
							else if (result3.rowCount === 1) {

								// include the user id and company id in insertion query values
								dbAttributeNames.push('from_userid');
								dbAttributeValues.push(result3.rows[0].id);

								dbAttributeNames.push('target_companyid');
								dbAttributeValues.push(companyId);

								// insert review into the db
								var query4 = 'insert into reviews (' + dbAttributeNames.join(',') + ') values (' + dbAttributeValues.join(',') + ');';

								client.query(query4, function(err2, result2) {

									if (err2) {
										console.log(err2);
										res.send( { "code" : 200, "data" : 'error'});
									}

									else if (result2.rowCount === 1) {
										res.send( { "code" : 1, "data" : 'review added'});
									}

									else {
										res.send( { "code" : 0, "data" : 'error saving review'});
									}

								});


							}

						});


					}


					// if there are results
					else {

						// write attributes into DB that are not already present
						res.send( { "code" : 2, "data" : 'error'});
					}


				});

			});
		}

		// session is not valid
		else {
			res.send( { "code" : 900, "data" : 'session invalid'});
		}

	}

	// first, check if session is valid
	isSessionValid(sessionid, sendSessionAndReviewInsertionConfirmationToClient);

};



// ************************************************
// ************************************************
//            PRODUCT
// ************************************************
// ************************************************


exports.productFindByCompanyId = function(req, res) {
	var id = req.params.id;
	console.log('Retrieving components of company: ' + id);
	db.collection('products', function(err, collection) {
		collection.find({'company_id':new BSON.ObjectID(id)}).toArray(function(err, items) {
			res.send(items);
		});
	});
};


exports.productsFindAll = function(req, res) {

//	var sessionid = validator.escape(req.query.sessionid);

	function sendSessionAndMethodResultToClient() {    // sResult

		// session is valid
//		if (sResult === true) {

			pg.connect(conString, function(err, client, done) {
				if(err) {
					res.send( { "code" : 3, "data" : 'pool error'});
					return console.error('error fetching client from pool', err);
				}
				client.query('SELECT products.id, products.name, products.description, companies.name as company_name, companies.id as company_id from products left join companies on products.company_id = companies.id', function(err, result) {
					//call `done()` to release the client back to the pool
					done();

					if(err) {
						res.send( { "code" : 0, "data" : 'query error'});
						return console.error('error running query', err);
					}

					if (result.rowCount===0) {
						res.send({'code' : 2, 'data' : 'no products found'});
					}

					if (result.rowCount>0) {
						res.send( { "code" : 1, "data" : result.rows});
					}
				});
			});

//		}
//		// session is not valid
//		else {
//			res.send( { "code" : 900, "data" : 'session invalid'});
//		}
	}

	sendSessionAndMethodResultToClient();
//	isSessionValid(sessionid, sendSessionAndMethodResultToClient);

};




//  db.collection('products', function(err, collection) {
//    console.log('Finding all products');
//    collection.find({}, {name:1, company_id:1, description:1}).toArray(function(err, items) {
//      res.send({ code: 1, data: items});
//    });
//  });


//    setTimeout(
//        function() {
//
//            db.collection('products', function(err, collection) {
//                console.log('Finding all products');
//                collection.find({}, {name:1, company_id:1, description:1}).toArray(function(err, items) {
//                    res.send({ code: 1, data: items});
//                });
//            });
//        },
//        2000
//    );
//};



exports.productFindById = function(req, res) {


//	var sessionid = validator.escape(req.query.sessionid);
//	console.log(sessionid);

	function sendSessionAndMethodResultToClient() { // sResult

		// session is valid
//		if (sResult === true) {

		var productId = req.params.productId;

		pg.connect(conString, function(err, client, done) {
			if(err) {
				res.send( { "code" : 3, "data" : 'pool error'});
				return console.error('error fetching client from pool', err);
			}
			client.query('SELECT products.name, products.description, ' +
				'companies.name as company_name, companies.id as company_id from products ' +
				'left join companies on companies.id = products.company_id ' +
				'where products.id = ' + productId,  function(err, result) {
				done();
				//call `done()` to release the client back to the pool


				if(err) {
					res.send( { "code" : 0, "data" : 'query error'});
					return console.error('error running query', err);
				}

				if (result.rowCount===0) {
					res.send({'code' : 2, 'data' : 'no product found'});
				}

				if (result.rowCount===1) {

					var objToReturn = {};
					objToReturn.product = result.rows[0];


					var reviewsQuery = 'select reviews.subject, reviews.text, reviews.rating, reviews.timestamp, reviews.anonymous_type, users.firstname as user_firstname, ' +
						'users.lastname as user_lastname, users.id as user_id from reviews left join users on reviews.from_userid = users.id where ' +
						'reviews.target_productid = ' + productId + ' and reviews.anonymous_type = 2 ' +
						'union ' +
						'select reviews.subject, reviews.text, reviews.rating, reviews.timestamp, reviews.anonymous_type, null as user_firstname, ' +
						'null as user_lastname, null as user_id from reviews left join users on reviews.from_userid = users.id where ' +
						'reviews.target_productid = ' + productId + ' and (reviews.anonymous_type IS NULL or reviews.anonymous_type = 1)';

					client.query(reviewsQuery, function(err2, result2) {
						done();

						if(err2) {
							res.send( { "code" : 1, "data" : objToReturn});
							return console.error('user message query error');
						}
						else {
							objToReturn.reviews = result2.rows;
							res.send( { "code" : 1, "data" : objToReturn});
						}
					});
				}

				// error - more than one product found for some reason
				else {
					res.send( { "code" : 4, "data" : 'error > 1 products returned'});
				}

			});
		});


//		}
		// session is not valid
//		else {
//			res.send( { "code" : 900, "data" : 'session invalid'});
//		}
	}

	sendSessionAndMethodResultToClient();
//	isSessionValid(sessionid, sendSessionAndMethodResultToClient);

};


exports.productAdd = function(req, res) {
	var component = req.body;
	console.log('Adding product: ' + JSON.stringify(component));
	db.collection('products', function(err, collection) {
		collection.insert(component, {safe:true}, function(err, result) {
			if (err) {
				res.send({'error':'An error has occurred'});
			} else {
				console.log('Success: ' + JSON.stringify(result[0]));
				res.send(result[0]);
			}
		});
	});
};


exports.productUpdate = function(req, res) {
	var id = req.params.id;
	var component = req.body;
	console.log('Updating product: ' + id);
	console.log(JSON.stringify(component));
	db.collection('products', function(err, collection) {
		collection.update({'_id':new BSON.ObjectID(id)}, { $set : component }, function(err, result) {
			if (err) {
				console.log('Error updating component: ' + err);
				res.send({'error':'An error has occurred'});
			} else {
				console.log('' + result + ' document(s) updated');
				res.send(component);
			}
		});
	});
};


// ************************************************
// ************************************************
//            REVIEWS
// ************************************************
// ************************************************


exports.reviewsFindAll = function(req, res) {


	var sessionid = validator.escape(req.query.sessionid);

	function sendSessionAndMethodResultToClient(sResult) {

		// session is valid
		if (sResult === true) {

			pg.connect(conString, function(err, client, done) {
				if(err) {
					res.send( { "code" : 3, "data" : 'pool error'});
					return console.error('error fetching client from pool', err);
				}
				client.query(
					'select users.firstname, users.lastname, reviews.from_userid as reviewer_userid, reviews.subject, ' +
						'reviews.id, reviews.timestamp, reviews.target_companyid, reviews.text, reviews.target_productid, companies.name as ' +
						'company_name, products.name as product_name from reviews left join companies on companies.id = ' +
						'reviews.target_companyid left join products on products.id = reviews.target_productid left join users on ' +
						'users.id = reviews.from_userid'
					, function(err, result) {
						//call `done()` to release the client back to the pool
						done();

						if(err) {
							res.send( { "code" : 0, "data" : 'query error'});
							return console.error('error running query', err);
						}

						if (result.rowCount===0) {
							res.send({'code' : 2, 'data' : 'no reviews found'});
						}

						if (result.rowCount>0) {
							res.send( { "code" : 1, "data" : result.rows});
						}
					});
			});
		}
		// session is not valid
		else {
			res.send( { "code" : 900, "data" : 'session invalid'});
		}
	}

	isSessionValid(sessionid, sendSessionAndMethodResultToClient);

};



// ************************************************
// ************************************************
//            SEARCH
// ************************************************
// ************************************************


exports.searchCompaniesProducts = function(req, res) {


	var searchString = req.body.searchstr;
	console.log(searchString);

	pg.connect(conString, function(err, client, done) {
		if(err) {
			res.send( { "code" : 3, "data" : 'pool error'});
			return console.error('error fetching client from pool', err);
		}
		client.query(
			"select companies.name as name, 'company' as type, companies.id as id from companies where companies.name ~* '.*" + searchString + ".*' OR companies.city ~* '.*" + searchString + ".*'" +
				"UNION select products.name as name, 'product' as type, products.id as id from products where products.name ~* '.*" + searchString + ".*'",
			function(err, result) {
				//call `done()` to release the client back to the pool
				done();

				if(err) {
					res.send( { "code" : 0, "data" : 'query error'});
					return console.error('error running query', err);
				}

				if (result.rowCount===0) {
					res.send({'code' : 2, 'data' : 'no results'});
				}

				if (result.rowCount>0) {
					res.send( { "code" : 1, "data" : result.rows});
				}
			});
	});


};


exports.oldSearchCompaniesProducts = function(req, res) {

	// var searchStringPattern = '/' + req.body.searchval + '/i';
	var searchStringPattern = replaceAll(' ', '|', req.body.searchstr);

	console.log(searchStringPattern);

	var regexObj = new RegExp(searchStringPattern, 'i');

	var returnObj = [];

	db.collection('companies', function(err, collection) {

		collection.find(
			{ $or :
				[
					{ 'name' : regexObj },
					{ 'city' : regexObj },
					{ 'country' : regexObj }
				]
			},
			{
				'name' : 1,
				'city' : 1,
				'country' : 1
			}
		).toArray(function(err, result) {

				if (err) {
					console.log('company search error');
					res.send({ code : 0, 'data' : err});
				}

				else {

					returnObj = result;

					db.collection('products', function(err, collection2) {
						collection2.find(
							{ $or : [
								{ 'name' : regexObj },
								{ 'description' : regexObj }
							]
							},
							{
								'name' : 1,
								'description' : 1,
								'company_id' : 1
							}
						).toArray(function(err2, result2) {
								if (err2) {
									console.log('product search error');
									res.send({ 'code' : 0, 'data' : err2});
								}
								else {

									for (var i=0; i<result2.length; i++) {
										returnObj.push(result2[i]);
									}

									res.send({'code' : 1, 'data' : returnObj });
								}
							});
					});


				}
			});
	});


};



function escapeRegExp(string) {
	return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}


function replaceAll(find, replace, str) {
	return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}


//exports.deleteUser = function(req, res) {
//    var id = req.params.id;
//    console.log('Deleting user: ' + id);
//    db.collection('users', function(err, collection) {
//        collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
//            if (err) {
//                res.send({'error':'An error has occurred - ' + err});
//            } else {
//                console.log('' + result + ' document(s) deleted');
//                res.send(req.body);
//            }
//        });
//    });
//}
